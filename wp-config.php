<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'scorp_acmo' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'jQFggf=WjNos%?;38XXnuJ5`.N.,IXr|BhBw5mr&x<%VLb ,O.%0h?RGuM;Xv%>G' );
define( 'SECURE_AUTH_KEY',  'M9>?3GYb9_VL RnF*j{(*$VuvK}#}!iM.Dw7#7/?b!21f_CVbHIc~:<<7g%o#/yy' );
define( 'LOGGED_IN_KEY',    'F,A8^BO3r~Q%UM[q=,m,7OzUz.# reXhf5x$RZVXgB^(}FBz^qr*.ZR=<G].E1_x' );
define( 'NONCE_KEY',        ';8U*,bU]bgAfTHt>s(>8q&.2bOj@B~.6;B+tK(_U5>cN:x#xqd1uxb ^bSsiA] z' );
define( 'AUTH_SALT',        '^d2}U-t+n<-kCHsez/+BVjUJ[ESet3hSS,X}70U$)0F/rZP `XG-N+|F46(}~f,`' );
define( 'SECURE_AUTH_SALT', 's/2Z}*?jA{G fFiR nWkj;v}lz;6#{|FrK3b%YpQEh@Yg+Pz-Hk:KdR#7P-=m1Sa' );
define( 'LOGGED_IN_SALT',   '<OzA(&g|XAXG8[7^Jmjh-wUA^[x_M^oJyb:]l$f/`#cK!C$;[6 /O:A`wAfwbrd8' );
define( 'NONCE_SALT',       '7W+a[.20Q.S;Bc%DnPom+KYb*P+D]p!@k:b%`x[OZ#;R-us61bY7lV?G=L]B/`FC' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

define( 'WP_HOME', 'https://localhost/webAcmo' );
define( 'WP_SITEURL', 'https://localhost/webAcmo' );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
